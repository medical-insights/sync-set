sync-set
========

Set with possibility to synchronize with other set content

Interface
---------

constructor(options)
	options:
		data [] - array of set items (optional)
		snapshot [] - array of set items from the last set synchronization (optional)

add(item)

remove(item)

has(item)

toJSON()
	return object with set content as lists of data and snapshot items:
		{
			data []
			snapshot []
		}

sync(toSet)
	synchronise content of the set with the set provided as the parameter