if (typeof define !== 'function') { var define = require('amdefine')(module) }

define(['underscore'], function(_){

	function SyncSet(options){
		var opts = options || {};
		this.data = {};
		this.snapshot = {};
		if(!_.isUndefined(opts.data) && _.isArray(opts.data)){
			_.each(opts.data, function(item){
				this.data[item] = true;
			}, this);
		}
		if(!_.isUndefined(opts.snapshot) && _.isArray(opts.snapshot)){
			_.each(opts.snapshot, function(item){
				this.snapshot[item] = true;
			}, this);
		}
	}

	SyncSet.prototype.add = function(item) {
		this.data[item] = true;
	};

	SyncSet.prototype.remove = function(item) {
		delete this.data[item];
	};

	SyncSet.prototype.has = function(item) {
		return !_.isUndefined(this.data[item]);
	};

	SyncSet.prototype.toJSON = function() {
		return {
			data : _.keys(this.data),
			snapshot : _.keys(this.snapshot)
		};
	};

	SyncSet.prototype.sync = function(toSet) {
		var jsonData = this.toJSON();
		var res = toSet._sync(jsonData.data, jsonData.snapshot);
		this.data = res;
		this.snapshot = _.clone(res);
	};

	SyncSet.prototype._sync = function(values, snapshot) {
		var ownValues = _.keys(this.data);
		var removed = _.difference(snapshot, ownValues);
		var added = _.difference(ownValues, snapshot);
		var res = _.difference(values, removed);
		res = _.union(res, added);
		var obj = {};
		_.each(res, function(item){
			obj[item] = true;
		});
		this.data = obj;
		this.snapshot = _.clone(obj);
		return _.clone(obj);
	};

	return SyncSet;
});
