var _ = require('underscore'),
    SyncSet = require('../index'),
    expect = require('chai').expect;

describe('SyncSet', function() {

	it('add/remove', function(){
		var ss = new SyncSet();
		ss.add(1);
		expect(ss.has(1)).to.be.true;
		expect(ss.has(0)).to.be.false;
		ss.add(1);
		expect(ss.has(1)).to.be.true;
		ss.remove(1);
		expect(ss.has(1)).to.be.false;
		ss.remove(1);
		expect(ss.has(1)).to.be.false;
	});

	it('sync to empty set changes nothing in origin', function(){
		var ss0 = new SyncSet();
		ss0.add(1);
		ss0.add(2);
		// console.log(ss0.toJSON());
		var ss1 = new SyncSet();
		// console.log(ss1.toJSON());
		expect(ss1.has(0)).to.be.false;
		expect(ss1.has(1)).to.be.false;
		expect(ss1.has(2)).to.be.false;
		ss0.sync(ss1);
		// console.log('-- sync --');
		// console.log(ss0.toJSON());
		// console.log(ss1.toJSON());
		expect(ss0.has(0)).to.be.false;
		expect(ss0.has(1)).to.be.true;
		expect(ss0.has(2)).to.be.true;

		expect(ss1.has(0)).to.be.false;
		expect(ss1.has(1)).to.be.true;
		expect(ss1.has(2)).to.be.true;
	});

	it('client changes reflected in server after sync', function(){
		var ss0 = new SyncSet();
		ss0.add(1);
		ss0.add(2);
		// console.log(ss0.toJSON());
		var ss1 = new SyncSet();
		ss0.sync(ss1);

		ss0.add(3);
		ss0.remove(1);

		// console.log(ss0.toJSON());
		// console.log(ss1.toJSON());
		ss0.sync(ss1);
		// console.log('-- sync --');
		// console.log(ss0.toJSON());
		// console.log(ss1.toJSON());
		expect(ss0.has(1)).to.be.false;
		expect(ss0.has(2)).to.be.true;
		expect(ss0.has(3)).to.be.true;

		expect(ss1.has(1)).to.be.false;
		expect(ss1.has(2)).to.be.true;
		expect(ss1.has(3)).to.be.true;
	});

	it('server changes reflected in the clinet after sync', function(){
		var ss0 = new SyncSet();
		ss0.add(1);
		ss0.add(2);
		// console.log(ss0.toJSON());
		var ss1 = new SyncSet();
		ss0.sync(ss1);

		ss1.add(3);
		ss1.remove(1);

		// console.log(ss0.toJSON());
		// console.log(ss1.toJSON());
		ss0.sync(ss1);
		// console.log('-- sync --');
		// console.log(ss0.toJSON());
		// console.log(ss1.toJSON());
		expect(ss0.has(1)).to.be.false;
		expect(ss0.has(2)).to.be.true;
		expect(ss0.has(3)).to.be.true;

		expect(ss1.has(1)).to.be.false;
		expect(ss1.has(2)).to.be.true;
		expect(ss1.has(3)).to.be.true;
	});

});